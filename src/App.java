import models.Time;

public class App {
    public static void main(String[] args) throws Exception {
        Time time1 = new Time();
        time1.setTime(7, 59, 59);
        System.out.println("Time 1: " + time1.toString());

        Time time2 = new Time();
        time2.setTime(18, 0, 0);
        System.out.println("Time 2: " + time2.toString());

        // Tăng time1 thêm 1s và giảm time2 đi 1s
        time1.nextSecond();
        time2.previousSecond();

        // In ra 2 obj time sau khi tăng và giảm
        System.out.println("After time1 tăng thêm 1s và time2 giảm đi 1s:");
        System.out.println("Time 1: " + time1.toString());
        System.out.println("Time 2: " + time2.toString());
    }
}
